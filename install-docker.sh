#!/bin/bash
echo "-------------------------------------------"
echo "effacement des anciennes versions de docker"
echo "-------------------------------------------"
sudo apt-get remove docker docker-engine docker.io containerd runc

echo "------"
echo "update"
echo "------"
sudo apt-get update

echo "-------------------------------------------"
echo "installation des paquets préalables par apt"
echo "-------------------------------------------"
sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release #détecte plus finement la version du système d'exploitation, nécessaire pour que le programme fonctionne sous mint, sous debian vous pouvez utiliser des uname normalement

echo "--------------------------------------------------------"
echo "clé de vérification des paquets depuis le site de docker"
echo "--------------------------------------------------------"
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo "----------------------------------------"
echo "configuration du sources.list.d selon OS"
echo "----------------------------------------"
MY_OS=$(lsb_release -is)
echo "Votre système d'exploitation est: " 
echo $MY_OS

case $MY_OS in 
# -------------------- Ubuntu ---------------------- #
     Ubuntu) echo \
             "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null;;
# -------------------- Debian ---------------------- #
     Debian) echo \
             "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null;;
# -------------------- Kali ---------------------- #
    Kali) echo "Bientôt implémenté"; #TODO: vérifier
          echo \
          "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null;;
# -------------------- LinuxMint ---------------------- #
    LinuxMint | Linuxmint) touch /tmp/distrib.txt ;
               head -n 3 /etc/upstream-release/lsb-release | tail -n 1 > /tmp/distrib.txt ;
               source /tmp/distrib.txt ;
               echo "La version Ubuntu de votre ";
               echo $MY_OS; #LinuxMint normalement
               echo " est ";
               echo $DISTRIB_CODENAME; #version d'Ubuntu trouvée dans /etc/upstream-release/lsb-release sous ce nom, ligne 3 normalement, si c'est pas à la bonne ligne mon code ne fonctionnera pas
               echo \
               "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $DISTRIB_CODENAME stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null ;
               unset $DISTRIB_CODENAME ; #nettoyage
               rm -f /tmp/distrib.txt ;; #nettoyage
    --) echo "erreur d'OS"; #TODO: lever exception
        break;;
    esac

unset MY_OS #nettoyage

echo "------"
echo "update"
echo "------"
sudo apt-get update

echo "---------------------------------------"
echo "installation des paquets docker par apt"
echo "---------------------------------------"
sudo apt-get install -y docker-ce docker-ce-cli containerd.io #si il y a écrit que les paquets n'ont pas été trouvés c'est que votre OS n'a pas été correctement détecté

echo "--------------------------------------------------"
echo "vérification que docker est installé (hello world)"
echo "--------------------------------------------------"
sudo docker run hello-world

echo "----------------------------------------------------------------------"
echo "installation docker-compose depuis le github officiel - version 2.24.6"
echo "----------------------------------------------------------------------"
#FIXME: Attention, penser à changer dans le code en demandant la version demandée ou en mettant à jour régulièrement
sudo curl -L "https://github.com/docker/compose/releases/download/v2.24.6/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose #peu importe votre OS précisément, il télécharge le paquet pour linux 32 ou 64 selon système

echo "-------------"
echo "et les droits"
echo "-------------"
sudo chmod +x /usr/local/bin/docker-compose

echo "--------------------------------------------------"
echo "vérification docker-compose est installé (version)"
echo "--------------------------------------------------"
docker-compose --version

echo "fin"
